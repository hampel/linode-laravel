Linode API Wrapper for Laravel
==============================

A Linode API wrapper for Laravel 5.x using Guzzle

By [Simon Hampel](http://hampelgroup.com/).

This package provides a simple Laravel service provider for our base Linode API wrapper package
[hampel/linode](https://bitbucket.org/hampel/linode) - please refer to the documentation about that package
for instructions on how to use this API wrapper

Installation
------------

The recommended way of installing Hampel Linode-Laravel is through [Composer](http://getcomposer.org):

Require the package via Composer in your `composer.json`

    :::json
    {
        "require": {
            "hampel/linode-laravel": "~3.0"
        }
    }

Run Composer to update the new requirement.

    :::bash
    $ composer update

The package is built to work with the Laravel 5 Framework.

Open your Laravel config file `app/config/app.php` and add the service provider in the `$providers` array:

    :::php
    'providers' => array(

        ...

        'Linode\LinodeServiceProvider'

    ),

You may also optionally add an alias entry to the `$aliases` array in the same file for the Linode facade:

	:::php
    "aliases" => array(

    	...

    	'Linode'			  => 'Linode\Facades\Linode',

    ),

Finally, to utilise the Linode API, you must generate an API key using the Linode manager and specify that key in
your `.env` file:

    :::bash
    LINODE_API_KEY=your_linode_api_key

Usage
-----

Use Laravel's App facade to gain access to the service provider in your code:

    :::php
    use Hampel\Linode\Commands\DomainCommand;

    $linode = App::make('linode');
    $domains = $linode->execute(new DomainCommand('list'));

... or chain them:

    :::php
    $domains = App::make('linode')->execute(new DomainCommand('list'));

... or just use the Facade instead:

    :::php
    $domains = Linode::execute(new DomainCommand('list'));

Refer to the usage examples and code in the [Linode API Wrapper](https://bitbucket.org/hampel/linode) repository for
more details about how to use the library.
