<?php namespace Linode;

use Hampel\Linode\Linode as LinodeApi;
use Illuminate\Support\ServiceProvider;

class LinodeServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bindShared('linode', function($app)
		{
			return LinodeApi::make($app['config']->get('linode.api_key'));
		});
	}

	public function boot()
	{
		$this->defineConfiguration();
	}

	protected function defineConfiguration()
	{
		$this->mergeConfigFrom(
			__DIR__ . '/config/linode.php', 'linode'
		);
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('linode');
	}

}
