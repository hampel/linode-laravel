<?php
/**
 * Configuration for Linode
 */

return array(

    /*
    |--------------------------------------------------------------------------
    | Linode API Key
    |--------------------------------------------------------------------------
    |
    | Specify the API Key for your Linode account
    |
    */

    'api_key' => env('LINODE_API_KEY', ''),

);
