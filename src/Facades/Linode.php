<?php namespace Linode\Facades;
/**
 *
 */

use Illuminate\Support\Facades\Facade;

class Linode extends Facade {

    protected static function getFacadeAccessor() { return 'linode'; }

}
