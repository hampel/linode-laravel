CHANGELOG
=========

3.0.1 (2015-05-23)
------------------

* removed redundant closing php tags

3.0.0 (2015-02-13)
------------------

* updated for Laravel 5.0 compatibility
* changed namespace to Linode\
* swapped to .env style configuration using a package config file rather than the services.php file
* updated to use hampel/linode v3.1

2.0.0 (2014-07-30)
------------------

* updated framework dependency to v4.2, hampel/linode to v3.0, and moved to psr-4 autoloading
* new simplified service provider - moved to Hampel\Linode namespace
* no longer need config file - use Laravel 4.2's services.php file for api key
* added Facade for Linode

1.3.1 (2014-06-01)
------------------

* less restrictive versioning of dependencies

1.3.0 (2014-06-01)
------------------

* updated framework requirement to 4.*
* removed dev requirement for phpunit
* updated README

1.2.0 (2013-12-14)
------------------

* removed orchestral/testbench from require-dev and removed ServiceProviderTest which used it due to a conflict in
  required classes
* updated requriements to use hampel/linode v2.0.* and adjusted service provider
* removed obsolete config entries

1.1.0 (2013-12-13)
------------------

* updated framework requirement to 4.1.*
* updated orchestra/testbench requirement to 2.1.*
* updated README

1.0.0 (2013-08-28)
------------------

* updated composer.json
* updated README
* added CHANGELOG
* added some unit tests

0.4.0 (2013-08-13)
------------------

* removed DomainHelper and LinodeException classes - decided it was best this functionality was implemented at
  application level
* removed resource helper too - best handled at application level

0.3.0 (2013-07-30)
------------------

* changed from using __callStatic to explicitly listing all getXX functions for resource types
* new function DomainHelper::createDomain()
* added cache keys as config variables; fixed problem with exception confusion; added code to catch exceptions from
  createDomain call
* added new refreshResourceCache function

0.2.0 (2013-07-19)
------------------

* added DomainHelper wrapper class for Domain packages
* new class ResourceHelper

0.1.0 (2013-07-15)
------------------

* initial release